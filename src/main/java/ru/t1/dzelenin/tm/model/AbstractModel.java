package ru.t1.dzelenin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Setter
@Getter
public abstract class AbstractModel {

    @Nullable
    private String id = UUID.randomUUID().toString();

}
