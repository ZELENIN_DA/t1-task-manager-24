package ru.t1.dzelenin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.ProjectSort;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final List<Project> projects = getProjectService().findAll(getUserId(), sort.getComparator());
        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            stringBuilder.append(index + ".");
            stringBuilder.append(project.getName() + " : ");
            stringBuilder.append(project.getDescription() + " : ");
            stringBuilder.append(project.getId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list projects.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
