package ru.t1.dzelenin.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email alredy exists...");
    }

    public ExistsEmailException(@NotNull final String email) {
        super("Error! Email '" + email + "'alredy exists...");
    }

}
